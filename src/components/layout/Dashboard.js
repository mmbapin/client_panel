import React from "react";
import Clients from "../clients/Clients";
import Sidebar from "./Sidebar";

const Dashboard = () => {
  return (
    <div className="row">
      <div className="col-xl-10">
        <Clients />
      </div>
      <div className="col-xl-2">
        <Sidebar />
      </div>
    </div>
  );
};

export default Dashboard;
